<?php

/**
 * Implements hook_views_plugins().
 */
function nooooo_views_plugins() {
  return array(
    'style' => array(
      'nooooo' => array(
        'title' => t('Nooooo!'),
        'help' => t('Omits wrapping divs for each row.'),
        'handler' => 'views_plugin_style',
        'theme' => 'nooooo_views_plugin_style',
        'theme path' => drupal_get_path('module', 'nooooo') . '/views/style',
        'theme file' => 'theme.inc',
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses grouping' => FALSE,
        'uses options' => FALSE,
        'type' => 'normal',
        // 'help topic' => 'nooooo-views-plugin-style',
        // @todo Write some documentation explaining that if you are using this
        //  style plugin, you should probably also be responsible for
        // implementing some template overrides for the View's rows. Otherwise
        // you'll have a mess of fields rendered one after the other with no
        // grouping.. Unless you like that sort of thing.
      ),
    ),
  );

}