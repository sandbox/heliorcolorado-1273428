<?php

/**
 * @file
 * This style plugin is similar to the "Naked" Panels style plugin, however
 * this is applicable to panel regions.
 */

$plugin = array(
  'title' => t('Nooooo!'),
  'description' => t('Omits wrapping markup.'),
  'render region' => 'nooooo_style_render_region',
);

/**
 * Theme function for panel regions.
 *
 * @ingroup themeable
 */
function theme_nooooo_style_render_region($vars) {
  $output = '';
  foreach ($vars['panes'] as $item) {
    $output .= $item;
  }
  return $output;
}
