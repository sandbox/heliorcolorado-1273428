<?php

/**
 * @file
 * This layout plugin has a single wrapping div for attributes.
 */

$plugin = array(
  'title' => t('Nooooo!'),
  'category' => t('Columns: 1'),
  'icon' => 'nooooo.png',
  'theme' => 'nooooo_panels_layouts',
  'regions' => array(
    'content' => t('Content'),
  ),
);

function template_preprocess_nooooo_panels_layouts(&$vars) {
  $display = $vars['display'];
  $vars['attributes_array']['class'] = $vars['classes_array'];
  
  if (isset($display->css_id) && $display->css_id) {
    $vars['attributes_array']['id'] = $display->css_id;
  }
}
