<?php

/**
 * @file
 * This layout plugin is absolutely bare and only outputs the single pane
 * region within it.
 */

$plugin = array(
  'title' => t('Noooooooooo!'),
  'category' => t('Columns: 1'),
  'icon' => 'noooooooooo.png',
  'theme' => 'nooooo_panels_layouts_bare',
  'regions' => array(
    'content' => t('Content'),
  ),
);
